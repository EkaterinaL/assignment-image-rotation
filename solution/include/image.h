#ifndef ASSIGNMENT_IMAGE_ROTATION_IMAGE_H
#define ASSIGNMENT_IMAGE_ROTATION_IMAGE_H

#include <stdint.h>

struct pixel {
    uint8_t b, g, r;
};

struct image {
    uint64_t width, height;
    struct pixel* data;
};

struct image create_image(uint32_t height, uint32_t width);

void free_image(struct image* img);

#endif //ASSIGNMENT_IMAGE_ROTATION_IMAGE_H
