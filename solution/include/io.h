#ifndef ASSIGNMENT_IMAGE_ROTATION_IO_H
#define ASSIGNMENT_IMAGE_ROTATION_IO_H

#include <stdbool.h>
#include <stdio.h>

bool open_file(char* filename, FILE** file, char* mode);

bool close_file(FILE** file);

#endif //ASSIGNMENT_IMAGE_ROTATION_IO_H
