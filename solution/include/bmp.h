#ifndef ASSIGNMENT_IMAGE_ROTATION_BMP_H
#define ASSIGNMENT_IMAGE_ROTATION_BMP_H

#include "image.h"
#include <stdio.h>

enum bmp_read_status{
    READ_OK = 0,
    READ_INVALID_HEADER,
    READ_INVALID_SIZE,
};

enum bmp_write_status {
    WRITE_OK=0,
    WRITE_HEADER_ERROR,
    WRITE_BODY_ERROR
};

#pragma pack(push, 1)
struct bmp_header {
    uint16_t bfType;
    uint32_t bfileSize;
    uint32_t bfReserved;
    uint32_t bOffBits;
    uint32_t biSize;
    uint32_t biWidth;
    uint32_t biHeight;
    uint16_t biPlanes;
    uint16_t biBitCount;
    uint32_t biCompression;
    uint32_t biSizeImage;
    uint32_t biXPelsPerMeter;
    uint32_t biYPelsPerMeter;
    uint32_t biClrUsed;
    uint32_t biClrImportant;
};
#pragma pack(pop)


enum bmp_read_status read_image_from_bmp(FILE* input, struct image* img);

struct bmp_header create_bmp_header(uint32_t width,uint32_t height);

enum bmp_write_status write_image_to_bmp(FILE *output, struct image *img);

#endif //ASSIGNMENT_IMAGE_ROTATION_BMP_H
