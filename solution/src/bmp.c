#include "../include/bmp.h"

const uint8_t padding_filler[] = {0,0,0,0};

static uint32_t calculate_padding(uint32_t width) {
    return (4 - (width * sizeof(struct pixel) % 4)) % 4;
}

struct bmp_header create_bmp_header(uint32_t width,uint32_t height) {
    uint8_t padding = calculate_padding(width);
    size_t size_image = (width*sizeof(struct pixel) + padding) * height;
    size_t file_size = sizeof(struct bmp_header) + size_image;
    struct bmp_header header = {
            .bfType = 19778,
            .bfileSize = file_size,
            .bfReserved = 0,
            .bOffBits = sizeof(struct bmp_header),
            .biSize = 40,
            .biWidth = width,
            .biHeight = height,
            .biPlanes = 1,
            .biBitCount = 24,
            .biCompression = 0,
            .biSizeImage = size_image,
            .biXPelsPerMeter = 2834,
            .biYPelsPerMeter = 2834,
            .biClrUsed = 0,
            .biClrImportant = 0
    };
    return header;
}

enum bmp_read_status read_image_from_bmp(FILE *input, struct image *img) {
    struct bmp_header header = {0};
    if (fread(&header, sizeof(struct bmp_header), 1, input) != 1) {
        return READ_INVALID_HEADER;
    }
    *img = create_image(header.biHeight, header.biWidth);

    size_t padding = calculate_padding(header.biWidth);

    for (uint32_t row = 0; row < header.biHeight; row++) {
        if (fread(img->data + (row * header.biWidth), sizeof(struct pixel), header.biWidth, input) != header.biWidth || // read row of image
            fseek(input, padding, SEEK_CUR) != 0) { // seek padding at the end of row
            free_image(img); // if something went wrong
            return READ_INVALID_SIZE;
        }
    }
    return READ_OK;
}

enum bmp_write_status write_image_to_bmp(FILE *output, struct image *img){
    struct bmp_header new_header = create_bmp_header(img->width,img->height);
    size_t padding_size = calculate_padding(new_header.biWidth);
    if(fwrite(&new_header, sizeof(struct bmp_header), 1, output) < 1 ) //writing header to file
        return WRITE_HEADER_ERROR;
    for (uint32_t row = 0; row < img->height; row++) {  //writing image to file
        if (fwrite(&img->data[row * img->width], sizeof(struct pixel), img->width, output)
            < img->width) return WRITE_BODY_ERROR;
        if(fwrite(&padding_filler,sizeof(uint8_t), padding_size,output) < padding_size) return WRITE_BODY_ERROR;
    }
    return WRITE_OK;
}
