#include "bmp.h"
#include "formatters.h"
#include "io.h"
#include <stdio.h>

int main( int argc, char** argv ) {
    if(argc!=3)
        return 1;

    FILE* input;
    FILE* output;
    struct image img;

    if (!open_file(argv[1],&input,"rb")) {
        printf("Failed opening file %s\n",argv[1]);
        return 1;
    };
    enum bmp_read_status read_status = read_image_from_bmp(input,&img);
    switch (read_status) {
        case READ_INVALID_SIZE:
            printf("Not enough data in bmp file\n");
            return 1;
        case READ_INVALID_HEADER:
            printf("Failed while reading bmp header");
            return 1;
        case READ_OK:
            printf("Successfully read bmp file");
    }

    if(!close_file(&input)){
        printf("Failed at closing input File");
    };

    struct image result = rotate(img);
    free_image(&img);

    if(!open_file(argv[2],&output,"wb")){
        printf("Failed opening file %s\n",argv[2]);
        return 1;
    };
    enum bmp_write_status write_status = write_image_to_bmp(output,&result);
    switch (write_status) {
        case WRITE_BODY_ERROR:
            printf("Error while writing body of bmp file\n");
            return 1;
        case WRITE_HEADER_ERROR:
            printf("Error while writing header of bmp file\n");
            return 1;
        case WRITE_OK:
            printf("Successfully wrote bmp file\n");
    }
    if(!close_file(&output)){
        printf("Failed at closing output File");
    };
    free_image(&result);

    return 0;
}
