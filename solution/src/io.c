#include "../include/io.h"

bool open_file(char* filename, FILE** file, char* mode){
    printf("Opening file\n");
    *file = fopen(filename,mode);
    return (*file)!=NULL;
}

bool close_file(FILE** file){
    return fclose(*file) == 0;
}
