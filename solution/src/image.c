#include "../include/image.h"
#include "malloc.h"

struct image create_image(uint32_t height, uint32_t width){
    return (struct image) {width, height, malloc(width * height * sizeof(struct pixel))};
}

void free_image(struct image* img){
    free(img->data);
}
