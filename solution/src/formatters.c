#include "../include/formatters.h"
#include "stdint.h"

struct image rotate(struct image img){
    struct image result_img = create_image(img.width,img.height);

    for (int row= 0; row< result_img.height; row++) {
        for (int column = 0; column < result_img.width; column++) {
            result_img.data[row* result_img.width + column] = img.data[
                    (result_img.width - column - 1) * img.width + row];
        }
    }
    return result_img;
}
